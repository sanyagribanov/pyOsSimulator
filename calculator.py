try:
    def math_operations(a, b, c):
        if c == 1:
            print(f"{a} + {b} = {a+b}")
        elif c == 2:
            print(f"{a} - {b} = {a-b}")
        elif c == 3:
            print(f"{a} * {b} = {a*b}")
        elif c == 4:
            print(f"{a} / {b} = {a/b}")
        elif c == 5:
            print(f"{a} ** {b} = {a**b}")
        else:
            print("Unknown command")

except ZeroDivisionError:
    print("Unavailable operation")
        
if __name__ == "__main__":
    num_1 = int(input("Enter 1st num: "))
    num_2 = int(input("Enter 2nd num: "))
    res = int(input("Choose math action:\n1 - +\n2 - -\n3 - *\n4 - /\n5 - ** \n"))
    math_operations(num_1, num_2, res)
      
    