def write_file():
    fileName = input("Введите имя и расширение файла, который хотите создать: ")
    f = open (f'{fileName}', 'w', encoding='utf-8')
    print("Справка: Для сохранения файла нажмите Ctrl+C\n")
    while True:
        try:
            line = input()
            f.write(line)
        except KeyboardInterrupt:
            break
    f.close()

def read_file():
    file_open = input("Введите имя и расширение файла, который хотите открыть: ")
    w = open(f'{file_open}','r', encoding='utf-8')
    print(w.read()) 
    w.close()

def choose_action():
    choose = input("Здравствуйте!\nКакое действие вы хотите выполнить?\n1 - записать\n2 - прочитать\n")
    if choose == "1":
        write_file()
    elif choose == "2":
        read_file()

if __name__ == "__main__":
   choose_action()
