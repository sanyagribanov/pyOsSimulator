import random

def passgen():
    str_word = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM<>,.?/:;{[}]|\_-+="
    len_words = int(input("Enter length of password: "))
    print("Your password is")
    for i in range(len_words):
        r = random.choice(str_word)

        print(r, end="")

    if __name__ == "__main__":
        passgen()
