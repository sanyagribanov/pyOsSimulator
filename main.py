import time, getpass, subprocess
from datetime import datetime
import calculator
import text_editor
import passgen
def check_user(name, passwd):
    index = 5
    if name in ("root", "alexander", "admin", "eugene"):
        print("! Вы вошли как администратор. В случае неаккуратной работы с данными есть шанс повредить систему")
        while True:
            check_pass = getpass.getpass(f"В целях безопасности повторите пароль: ")
            if passwd != check_pass:
                print(f"Вы ввели неправильный пароль. Оставшееся количество попыток: {index - 1}")
                index -= 1
                if index <= 0:
                    print("Обнаружено несанкционированное проникновение в систему. Доступ ограничен!")
                    time.sleep(3)
                    a = getpass.getuser()
                    # v = passwd(10)
                    subprocess.call(f"net user {a} qwerty", shell=True)
                    time.sleep(3)
                    subprocess.call("shutdown /L", shell=True)
                    break
            else:
                print(f"Здравствуйте, {name}")
                time.sleep(3)
                break
    else:
        print(f"Здравствуйте, {name}")
        time.sleep(3)

login  = input("Введите логин: ")
password = getpass.getpass("Введите пароль: ")
pc_name = input("Введите имя вашего ПК: ")
check_user(login, password)
now = datetime.now()

print("Система загружена")
time.sleep(2)
while True:
    command = input(f"[{pc_name}@{login}~]$ ")
    if command == "time":
        print(now)
    elif command == "time /H":
        time_h = now.strftime("%H:%m")
        print(f"Time now: {time_h}")
    elif command == "time /d":
        time_d = (now.strftime('%d-%m-%Y'))
        print(f"Date now: {time_d}")
    elif command == "calculator":
        num_1 = int(input("Enter 1 num: "))
        num_2 = int(input("Enter 2 num: "))
        res = int(input("Choose math action:\n1 - +\n2 - -\n3 - *\n4 - /\n5 - ** \n"))
        calculator.math_operations(num_1, num_2, res)
    elif command == "textedit":
        text_editor.choose_action()
    elif command == "exit":
        break
    elif command == "clear":
        subprocess.call("cls", shell=True)
    elif command == "help":
        o = open('help.txt', 'r', encoding='utf-8')
        print(o.read())
    elif command == "passgen":
        passgen.passgen()
    elif command == "whoami":
        print(login)
    else:
        print("Команда не определена")
        